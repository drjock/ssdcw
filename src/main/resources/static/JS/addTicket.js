function loadPage() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("username").innerHTML = "Hello " + xhttp.responseText;
        }
    };
    xhttp.open("GET", "/myname", true);
    xhttp.send();
}


function encodeHTML(s) {
    return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;');
}

function newTicket() {
    var title = encodeHTML(document.getElementById("title").value);
    var assignedTo = encodeHTML(document.getElementById("assignee").value);
    var description = encodeHTML(document.getElementById("description").value);
    var priority = document.getElementById("Priority").value;
    var type = document.getElementById("type").value;
    var xmlhttp = new XMLHttpRequest();
    var theUrl = "/ticket";
    xmlhttp.open("POST", theUrl);
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.send(JSON.stringify({
        title,
        assignedTo,
        description,
        priority,
        type
    }));
}

