function loadPage() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("username").innerHTML = "Hello " + xhttp.responseText;
        }
    };
    xhttp.open("GET", "/myname", true);
    xhttp.send();

    loadTable();
}

function loadTable() {

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            loadTableData(JSON.parse(xhttp.response))
        }
    };

    xhttp.open("GET", "/ticket", true);
    xhttp.send();


}

function loadTableData(items) {
    const table = document.getElementById("tickets");
    for (var i = 0; i < items.length; i++) {
        let item = items[i];
        let username = getUsername(item.assignedTo);
        let row = table.insertRow();
        let title = row.insertCell(0);
        title.innerHTML = item.title;
        let description = row.insertCell(1);
        description.innerHTML = item.description;
        let date = row.insertCell(2);
        date.innerHTML = item.dateCreated;
        let assignee = row.insertCell(3);
        getUsername(assignee,item.assignedTo)
        let creator = row.insertCell(4);
        creator.innerHTML = item.createdBy;
        let type = row.insertCell(5);
        type.innerHTML = item.type;
        let priority = row.insertCell(6);
        priority.innerHTML = item.priority;
        let status = row.insertCell(7);
        status.innerHTML = item.status;
        let link = row.insertCell(8);
        link.innerHTML = link.innerHTML + "<a href=ticket.html?id=" + item.id + ">View ticket</a>";
    }
}

function getUsername(assignee,userID) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
             assignee.innerHTML =  xhttp.responseText;
        }
    };
    xhttp.open("POST", "/user", true);
    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhttp.send(JSON.stringify({
                       userID
                   }));
}
