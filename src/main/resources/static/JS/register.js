
function encodeHTML(s) {
    return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;');
}

function newUser(e) {
var username = encodeHTML(document.getElementById("username").value);
var password = encodeHTML(document.getElementById("password").value);
var passwordConfirm = encodeHTML(document.getElementById("passwordConfirm").value);
var xmlhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("text").innerHTML = "Account created you can login with this account now :-)";
                        }
            if (this.readyState == 4 && this.status == 406) {
                document.getElementById("text").innerHTML = "Ensure that both passwords match and that the password is at least 10 characters long, contain at least one upper and lower case letter, contain a number and a special character";
            }
        };
var theUrl = "/registration";
xmlhttp.open("POST", theUrl);
xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
xmlhttp.send(JSON.stringify({ username,password }));
}


