function loadPage() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("username").innerHTML = "Hello " + xhttp.responseText;
        }
    };
    xhttp.open("GET", "/myname", true);
    xhttp.send();

    loadTable();
}

function getURLParams() {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get('id');
}

function loadTable() {
    const urlParams = new URLSearchParams(window.location.search);
    const id = getURLParams();
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            loadTableData(JSON.parse(xhttp.response))
        }
    };
    xhttp.open("POST", "/singleTicket", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify({
        id
    }));
}

function loadTableData(item) {
    const table = document.getElementById("ticket");
    const commentTable = document.getElementById("comments");
    let row = table.insertRow();
    let title = row.insertCell(0);
    title.innerHTML = item.title;
    let description = row.insertCell(1);
    description.innerHTML = item.description;
    let date = row.insertCell(2);
    date.innerHTML = item.dateCreated;
    let assignee = row.insertCell(3);
    getUsername(assignee, item.assignedTo);
    let creator = row.insertCell(4);
    creator.innerHTML = item.createdBy;
    let type = row.insertCell(5);
    type.innerHTML = item.type;
    let priority = row.insertCell(6);
    priority.innerHTML = item.priority;
    let status = row.insertCell(7);
    status.innerHTML = item.status;
    for (var i = 0; i < item.comments.length; i++) {
        let userComment = item.comments[i];
        let commentRow = commentTable.insertRow();
        let author = commentRow.insertCell(0);
        author.innerHTML = userComment.writtenBy;
        let comment = commentRow.insertCell(1);
        comment.innerHTML = userComment.comment;
    }
}

function getUsername(row, userID) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            row.innerHTML = xhttp.responseText;
        }
    };
    xhttp.open("POST", "/user", true);
    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhttp.send(JSON.stringify({
        userID
    }));
}

function addComment() {
    const id = getURLParams();
    const comment = encodeHTML(document.getElementById("comment").value);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            window.location.href = "ticket.html?id=" + id;
        }
        if (this.readyState == 4 && this.status == 406) {
            window.location.href = "ticket.html?id=" + id;
            document.getElementById("commentMessage").innerHTML = "This ticket is closed";
        }
    };
    xhttp.open("POST", "/comment", true);
    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhttp.send(JSON.stringify({
        id,
        comment
    }));

}

function updateStatus() {
    const id = getURLParams();
    var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                     window.location.reload();
                        }
            if (this.readyState == 4 && this.status == 406) {
                document.getElementById("statusMessage").innerHTML = "Only the user assigned to this ticket can update the status";
            }
        };
    xhttp.open("POST", "/status", true);
    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhttp.send(JSON.stringify({
        id
    }));
}


function encodeHTML(s) {
    return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;');
}