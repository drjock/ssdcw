function update (){
var password = encodeHTML(document.getElementById("password").value);
var passwordConfirm = encodeHTML(document.getElementById("passwordConf").value);
if(password === passwordConfirm){
var xhttp = new XMLHttpRequest();
var theUrl = "/updatePass";
    xhttp.onreadystatechange = function() {

      if (this.readyState == 4 && this.status == 200) {
            window.location.href = "index.html";
        }
          if (this.readyState == 4 && this.status == 403) {
                document.getElementById("text").innerHTML = "Ensure that both passwords match and that the password is at least 10 characters long, contain at least one upper and lower case letter, contain a number and a special character";
                }
    };
xhttp.open("POST", theUrl);
xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
xhttp.send(JSON.stringify({password}));
}
else {
document.getElementById("text").innerHTML = "Ensure that passwords match";
}
}



function encodeHTML(s) {
    return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;');
}