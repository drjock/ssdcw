package com.example.ssdcw.controllers;

import com.example.ssdcw.database.UserRepository;
import com.example.ssdcw.entities.User;
import com.example.ssdcw.utils.UserHelper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.regex.Pattern;

@RestController
public class RegistrationController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/registration")
    public ResponseEntity createNewUser (@RequestBody ObjectNode json){
        if (UserHelper.isValid(json.get("password").asText())&& userRepository.findByUsername(json.get("username").asText()) == null) {
            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            User newUser = new User();
            newUser.setUsername(json.get("username").asText());
            newUser.setPasswordHash(passwordEncoder.encode(json.get("password").asText()));
            userRepository.save(newUser);
            return new ResponseEntity(HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        }
    }


}
