package com.example.ssdcw.controllers;

import com.example.ssdcw.database.TicketRepository;
import com.example.ssdcw.database.UserRepository;
import com.example.ssdcw.entities.*;
import com.example.ssdcw.utils.UserHelper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class TicketController {

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/ticket")
    public List<Ticket> getTickets () {
        return ticketRepository.findAll();
    }

    @PostMapping("/ticket")
    public ResponseEntity addTicket (@RequestBody ObjectNode json ) {
        if (ticketRepository.findByTitle(json.get("title").asText() )== null) {
            Ticket newTicket = new Ticket();
            newTicket.setCreatedBy(UserHelper.getLoggedInUser());
            newTicket.setAssignedTo(userRepository.findByUsername(json.get("assignedTo").asText()).getId());
            newTicket.setDescription(json.get("description").asText());
            newTicket.setPriority(TicketPriority.valueOf(json.get("priority").asText()));
            newTicket.setTitle(json.get("title").asText());
            newTicket.setType(TicketType.valueOf(json.get("type").asText()));
            ticketRepository.save(newTicket);
            return new ResponseEntity(HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @PostMapping("/singleTicket")
    public Ticket getTicketById (@RequestBody ObjectNode json){
        return ticketRepository.findByid(json.get("id").asText());
    }

    @PostMapping ("/comment")
    public ResponseEntity addComment(@RequestBody ObjectNode json) {

        Ticket existingTicket = ticketRepository.findByid(json.get("id").asText());
        if (existingTicket.getStatus() != TicketStatus.CLOSED){
        Comment comment = new Comment(json.get("comment").asText(), UserHelper.getLoggedInUser());

        existingTicket.getComments().add(comment);
        ticketRepository.save(existingTicket);

        return new ResponseEntity(HttpStatus.OK);}
        else {
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @PostMapping("/status")
    public ResponseEntity updateTicketStatus (@RequestBody ObjectNode json) {
        String id = json.get("id").asText();
        Ticket ticket = ticketRepository.findByid(id);
        if (userRepository.findByid(ticket.getAssignedTo()).getUsername().equals(UserHelper.getLoggedInUser())) {
        switch (ticket.getStatus()) {
            case OPEN:
                ticket.setStatus(TicketStatus.RESOLVED);
                break;
            case RESOLVED:
                ticket.setStatus(TicketStatus.CLOSED);
                break;
            case CLOSED:
                ticket.setStatus(TicketStatus.OPEN);
                break;
        }
        ticketRepository.save(ticket);
        return new  ResponseEntity(HttpStatus.OK);
        }
        else {
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        }
    }



}
