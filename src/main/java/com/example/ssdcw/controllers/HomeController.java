package com.example.ssdcw.controllers;

import com.example.ssdcw.database.UserRepository;
import com.example.ssdcw.entities.Ticket;
import com.example.ssdcw.entities.User;
import com.example.ssdcw.utils.UserHelper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
public class HomeController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping(path = "/myname")
    @ResponseBody
    public String currentUserName(Principal principal) {
        return userRepository.findByUsername(UserHelper.getLoggedInUser()).getUsername();
    }

    @PostMapping("/updatePass")
    public ResponseEntity updatePassword (@RequestBody ObjectNode json){
        if (UserHelper.isValid(json.get("password").asText())) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            User user = userRepository.findByUsername(UserHelper.getLoggedInUser());
            user.setPasswordHash(passwordEncoder.encode(json.get("password").asText()));
            userRepository.save(user);
            return new ResponseEntity(HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        }

    }

    @PostMapping("/user")
    public String getUserByID (@RequestBody ObjectNode json){
        if (json.get("userID") != null) {
            return userRepository.findByid(json.get("userID").asText()).getUsername();
        }
    else{return null;}
    }


    }