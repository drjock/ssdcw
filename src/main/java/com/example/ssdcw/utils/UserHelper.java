package com.example.ssdcw.utils;

import com.example.ssdcw.entities.User;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.regex.Pattern;

public class UserHelper {

    public static String getLoggedInUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return((UserDetails) principal).getUsername();
        } else {
            return  principal.toString();
        }
    }

    public static boolean isValid(String password) {

        Pattern specialCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Pattern UpperCasePatten = Pattern.compile("[A-Z ]");
        Pattern lowerCasePatten = Pattern.compile("[a-z ]");
        Pattern digitCasePatten = Pattern.compile("[0-9 ]");

        boolean isPasswordValid=true;

        if (password.length() < 10) {
            isPasswordValid=false;
        }
        if (!specialCharPatten.matcher(password).find()) {
            isPasswordValid=false;
        }
        if (!UpperCasePatten.matcher(password).find()) {
            isPasswordValid=false;
        }
        if (!lowerCasePatten.matcher(password).find()) {
            isPasswordValid=false;
        }
        if (!digitCasePatten.matcher(password).find()) {
            isPasswordValid=false;
        }

        return isPasswordValid;

    }

}
