package com.example.ssdcw.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashSet;
import java.util.Set;

@Document(collection="User")
public class User {

    @Id
    private String id ;

    private String username;

    private String password;

    private int loginAttempts;

    private Boolean accountLocked;

    private Set<String> roles = new HashSet<String>();

    public void addRole(String role) {
        roles.add(role);
    }

    public Set<String> getRoles() {
        return roles;
    }

    public User() {
        this.accountLocked = false;
        this.roles = new HashSet<>();
        this.roles.add("ROLE_USER");
    }

    public User(String username, String passwordHash, Boolean accountLocked) {
        this.username = username;
        this.password = passwordHash;
        this.accountLocked = accountLocked;
    }

    public String getId() {
        return id;
    }

    public Boolean getAccountLocked() {
        return accountLocked;
    }

    public void setAccountLocked(Boolean accountLocked) {
        this.accountLocked = accountLocked;
    }

    public String getPassword() {
        return password;
    }

    public void updatePassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public void lockAccount () {
        this.accountLocked = true;
    }

    public void unlockAccount () {
        this.accountLocked = false;
    }

    public void setPasswordHash(String passwordHash) {
        this.password = passwordHash;
    }

    public int getLoginAttempts() {
        return loginAttempts;
    }

    public void setLoginAttempts(int loginAttempts) {
        this.loginAttempts = loginAttempts;
    }
}
