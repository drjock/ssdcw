package com.example.ssdcw.entities;

public enum  TicketStatus {

    OPEN,
    RESOLVED,
    CLOSED
}
