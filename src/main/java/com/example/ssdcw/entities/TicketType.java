package com.example.ssdcw.entities;

public enum TicketType {
    DEV,
    TEST,
    PROD
}
