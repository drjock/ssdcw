package com.example.ssdcw.entities;

public enum TicketPriority {

    LOW,
    MEDIUM,
    HIGH
}
