package com.example.ssdcw.entities;

import org.springframework.data.annotation.Id;

public class Comment {
    @Id
    private String id;

    private String comment;

    private String writtenBy;

    public Comment(String comment, String writtenBy) {
        this.comment = comment;
        this.writtenBy = writtenBy;
    }

    public String getId() {
        return id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getWrittenBy() {
        return writtenBy;
    }

    public void setWrittenBy(String writtenBy) {
        this.writtenBy = writtenBy;
    }
}
