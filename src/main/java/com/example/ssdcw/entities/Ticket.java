package com.example.ssdcw.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document(collection="Ticket")
public class Ticket {

    @Id
    private String id;

    private String title;

    private String description;

    private String createdBy;

    private String assignedTo;

    private Date dateCreated;

    private TicketType type;

    private TicketPriority priority;

    private TicketStatus status;

    private List<Comment> comments;

    public Ticket(String title, String description, String createdBy, String assignedTo, Date dateCreated, TicketType type, TicketPriority priority, TicketStatus status, List<Comment> comments) {
        this.title = title;
        this.description = description;
        this.createdBy = createdBy;
        this.assignedTo = assignedTo;
        this.dateCreated = dateCreated;
        this.type = type;
        this.priority = priority;
        this.status = status;
        this.comments = comments;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public TicketType getType() {
        return type;
    }

    public void setType(TicketType type) {
        this.type = type;
    }

    public TicketPriority getPriority() {
        return priority;
    }

    public void setPriority(TicketPriority priority) {
        this.priority = priority;
    }

    public TicketStatus getStatus() {
        return status;
    }

    public void setStatus(TicketStatus status) {
        this.status = status;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment>  comments) {
        this.comments = comments;
    }

    public Ticket() {
        this.comments = new ArrayList<>();
        this.status = TicketStatus.OPEN;
        this.dateCreated = new Date();
    }

}
