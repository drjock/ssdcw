package com.example.ssdcw.database;

import com.example.ssdcw.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<User,String> {
     List<User> findAll();
     User findByUsername(String username);
     User findByid (String id);
     User save(User user);
     void delete(User user);
}
