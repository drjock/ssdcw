package com.example.ssdcw.database;

import com.example.ssdcw.entities.Ticket;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends MongoRepository<Ticket, String>  {
     List<Ticket> findAll();
     Ticket findByid(String id);
     Ticket findByTitle (String title);
     Ticket save(Ticket user);
     void delete(Ticket user);
}
